package steps;

import com.codeborne.selenide.Selenide;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.cart.CartPage;
import pages.home.HomePage;
import pojos.OrderInfo;

import java.util.List;
import java.util.Map;

public class PurchaseSteps {

    private HomePage homePage;
    private CartPage cartPage;

    @Before
    public void init() {
        homePage = new HomePage();
        cartPage = new CartPage();
    }

    @Given("I have opened the home page")
    public void iHaveOpenedTheHomePage() {
        this.homePage.openPage();
    }

    @And("navigate to {string} category")
    public void navigateToCategory(String category) {
        this.homePage.openProductCategory(category);
    }

    @And("select product {string}")
    public void selectProduct(String productName) {
        this.homePage.selectProduct(productName);
    }

    @And("add the product to the cart")
    public void addTheProductToTheCart() {
        this.homePage.addToCart();
    }

    @When("click on {string} section")
    public void clickOnButton(String sectionName) {
        this.homePage.selectSection(sectionName);
    }

    @And("delete the product {string} from the cart")
    public void deleteTheProductFromTheCart(String productName) {
        this.cartPage.deleteProduct(productName);
    }

    @And("click on place order button")
    public void clickOnPlaceOrderButton() {
        this.cartPage.placeOrder();
    }

    @And("fill in order information")
    public void fillInOrderInformation(DataTable orderInfo) {

        List<Map<String, String>> rows = orderInfo.asMaps(String.class, String.class);

        OrderInfo orderInfoData = null;

        for (Map<String, String> form : rows) {
            orderInfoData = OrderInfo.builder()
                    .name(form.get("name"))
                    .country(form.get("country"))
                    .city(form.get("city"))
                    .creditCard(form.get("card"))
                    .month(form.get("month"))
                    .year(form.get("year"))
                    .build();

        }

        assert orderInfoData != null;
        this.cartPage.fillInOrderInfo(orderInfoData);
    }

    @And("click on purchase button")
    public void clickPurchaseButton() {
        this.cartPage.purchaseOrder();
    }

    @Then("order with amount {string} should be successfully created")
    public void orderWithAmountShouldBeSuccessfullyCreated(String amount) {
        this.cartPage.cartPageAssertions().purchaseSuccessful(amount);
        this.cartPage.clickOkButton();
    }

    @Then("total price in cart should be {int}")
    public void totalPriceInCartShouldBe(int totalPrice) {
        this.cartPage.cartPageAssertions().totalPriceIsCorrect(String.valueOf(totalPrice));
    }

    @And("count of the products in cart should be {int}")
    public void countOfProductsInCartShouldBe(int productsSize) {
        this.cartPage.cartPageAssertions().countOfProductsIsCorrect(productsSize);
    }

    @After
    public void close() {
        Selenide.closeWebDriver();
    }
}
