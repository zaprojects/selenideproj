Feature: Product purchase

  Background:
    Given I have opened the home page

  @smoke
  Scenario: Add products in cart
    Given navigate to "Phones" category
    And select product "Nexus 6"
    And add the product to the cart
    And click on "Home" section
    And navigate to "Phones" category
    And select product "Samsung Galaxy S7"
    And add the product to the cart
    When click on "Cart" section
    Then total price in cart should be 1450
    And count of the products in cart should be 2

  @smoke
  Scenario: Add and delete a products from cart
    Given navigate to "Monitors" category
    And select product "Apple monitor 24"
    And add the product to the cart
    And click on "Home" section
    And navigate to "Monitors" category
    And select product "ASUS Full HD"
    And add the product to the cart
    And click on "Cart" section
    When delete the product "ASUS Full HD" from the cart
    Then total price in cart should be 400
    And count of the products in cart should be 1

  @full
  Scenario: Purchase products
    Given navigate to "Laptops" category
    And select product "Sony vaio i5"
    And add the product to the cart
    And click on "Home" section
    And navigate to "Laptops" category
    And select product "Dell i7 8gb"
    And add the product to the cart
    And click on "Cart" section
    And delete the product "Dell i7 8gb" from the cart
    When click on place order button
    And fill in order information
      | name | country | city  | card             | month | year |
      | Jack | Belarus | Minsk | 4111111111111111 | 02    | 2021 |
    And click on purchase button
    Then order with amount "790 USD" should be successfully created


