package base;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$x;

public class BasePageMap {

    public SelenideElement selectSection(String sectionButtonName) {
        return $x("//ul[@class='navbar-nav ml-auto']")
                .findAll(By.xpath("li[contains(@class, 'nav-item')]/a")).findBy(text(sectionButtonName));
    }
}
