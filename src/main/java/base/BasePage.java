package base;

import com.codeborne.selenide.Configuration;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public abstract class BasePage<T> {

    public abstract String getUrl();
    private WebDriverWait wait;

    public T openPage() {
        Configuration.startMaximized = true;
        open(getUrl());
        return (T)this;
    }

    public void waitAlertIsPresent() {
        this.wait = new WebDriverWait(getWebDriver(), 5);
        this.wait.until(ExpectedConditions.alertIsPresent());
    }
}
