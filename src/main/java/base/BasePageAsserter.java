package base;

public class BasePageAsserter<TM, T extends BasePage> {

    protected TM elementMapInstance;
    protected T pageClassInstance;

    public BasePageAsserter(TM elementMapClass, T pageClassInstance) {
        this.elementMapInstance = elementMapClass;
        this.pageClassInstance = pageClassInstance;
    }
}
