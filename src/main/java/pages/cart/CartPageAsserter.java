package pages.cart;

import base.BasePageAsserter;
import com.codeborne.selenide.SelenideElement;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;

import java.time.Duration;
import java.util.Arrays;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Condition.appear;

public class CartPageAsserter extends BasePageAsserter<CartPageMap, CartPage> {

    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(CartPageAsserter.class);

    public CartPageAsserter(CartPageMap elementMapClass, CartPage pageClassInstance) {
        super(elementMapClass, pageClassInstance);
    }

    public CartPageAsserter purchaseSuccessful(String amount) {

        SelenideElement purchaseAlert = this.elementMapInstance.purchaseAlertDetails().should(appear, Duration.ofSeconds(11));

        String orderId = getTextByLines(purchaseAlert.getText(), 0);
        String orderAmount = getTextByLines(purchaseAlert.getText(), 1);

        log.info("Order with " + orderId + " and " + orderAmount + " has been created.");

        String modifiedAmount = StringUtils.substringAfter(orderAmount, "Amount: ");

        Assertions.assertAll(
                () -> Assertions.assertEquals("Thank you for your purchase!",
                        this.elementMapInstance.purchaseAlertMessage().getText()),
                () -> Assertions.assertEquals(amount, modifiedAmount)
        );

        return this;
    }

    public CartPageAsserter totalPriceIsCorrect(String expectedPrice) {
        Assertions.assertEquals(expectedPrice, this.elementMapInstance.cartTotalPrice().should(appear).getText());
        return this;
    }

    public CartPageAsserter countOfProductsIsCorrect(int products) {
        Assertions.assertEquals(products, this.elementMapInstance.productsInCart().size());
        return this;
    }

    private String getTextByLines(String text, int lineNumber) {
        return Arrays.stream(text.split("\n"))
                .collect(Collectors.toList())
                .get(lineNumber);
    }
}
