package pages.cart;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.$x;

public class CartPageMap {

    public ElementsCollection productsInCart() {
        return $$("#tbodyid tr");
    }

    public SelenideElement placeOrderButton() {
        return Selenide.$x("//button[@type='button' and @data-target='#orderModal']");
    }

    public SelenideElement productInCart(String productName) {
        return $x(String.format("//tbody[@id='tbodyid']/tr/td/following::td[contains(text(), '%s')]", productName));
    }

    public SelenideElement deleteProduct(String productName) {
        return productInCart(productName).find(By.xpath("following::td/a[starts-with(@onclick, 'deleteItem')]"));
    }

    public SelenideElement nameField() {
        return $("#name");
    }

    public SelenideElement countryField() {
        return $("#country");
    }

    public SelenideElement cityField() {
        return $("#city");
    }

    public SelenideElement cardField() {
        return $("#card");
    }

    public SelenideElement monthField() {
        return $("#month");
    }

    public SelenideElement yearField() {
        return $("#year");
    }

    public SelenideElement purchaseButton() {
        return $("button[onclick*=purchaseOrder]");
    }

    public SelenideElement purchaseAlertMessage() {
        return $x("//div[@class='sa-icon sa-custom']/following::h2");
    }

    public SelenideElement purchaseAlertDetails() {
        return $x("//p[@class=\"lead text-muted \"]");
    }

    public SelenideElement okButton() {
        return $("button[class*=confirm]");
    }

    public SelenideElement cartTotalPrice() {
        return $("#totalp");
    }

}
