package pages.cart;

import base.BasePage;
import pojos.OrderInfo;

import static com.codeborne.selenide.Condition.appear;
import static com.codeborne.selenide.Condition.exist;

public class CartPage extends BasePage<CartPage> {

    @Override
    public String getUrl() {
        return "https://www.demoblaze.com/cart.html";
    }

    public CartPageAsserter cartPageAssertions() {
        return new CartPageAsserter(new CartPageMap(), new CartPage());
    }

    protected CartPageMap cartPageElements() {
        return new CartPageMap();
    }

    public CartPage placeOrder() {
        this.cartPageElements().placeOrderButton().click();
        return this;
    }
    public CartPage deleteProduct(String productName) {
        this.cartPageElements().deleteProduct(productName).click();
        this.cartPageElements().productInCart(productName).shouldNot(exist);
        this.cartPageElements().cartTotalPrice().should(appear);
        return this;
    }

    public CartPage fillInOrderInfo(OrderInfo orderInfo) {
        this.cartPageElements().nameField().click();
        this.cartPageElements().nameField().sendKeys(orderInfo.getName());
        this.cartPageElements().countryField().sendKeys(orderInfo.getCountry());
        this.cartPageElements().cityField().sendKeys(orderInfo.getCity());
        this.cartPageElements().cardField().sendKeys(orderInfo.getCreditCard());
        this.cartPageElements().monthField().sendKeys(orderInfo.getMonth());
        this.cartPageElements().yearField().sendKeys(orderInfo.getYear());

        return this;
    }

    public CartPage purchaseOrder() {
        this.cartPageElements().purchaseButton().click();
        return this;
    }

    public CartPage clickOkButton() {
        this.cartPageElements().okButton().click();
        return this;
    }

}
