package pages.home;

import base.BasePage;

import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class HomePage extends BasePage<HomePage> {

    protected HomePageMap homePageElements() {
        return new HomePageMap();
    }

    @Override
    public String getUrl() {
        return "https://www.demoblaze.com/index.html";
    }

    public HomePage openProductCategory(String categoryName) {
        this.homePageElements().productCategory(categoryName).click();
        return this;
    }

    public HomePage selectProduct(String productName) {
        this.homePageElements().selectProduct(productName).click();
        return this;
    }

    public HomePage selectSection(String sectionName) {
        this.homePageElements().selectSection(sectionName).click();
        return this;
    }

    public HomePage addToCart() {
        this.homePageElements().addToCartButton().click();
        this.waitAlertIsPresent();
        getWebDriver().switchTo().alert().accept();
        return this;
    }
}
