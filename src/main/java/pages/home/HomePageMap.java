package pages.home;

import base.BasePageMap;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class HomePageMap extends BasePageMap {

    public SelenideElement productCategory(String productCategory) {
        return $$(".list-group-item").findBy(text(productCategory));
    }

    public SelenideElement selectProduct(String productName) {
        return $$(".hrefch").findBy(text(productName));
    }

    public SelenideElement addToCartButton() {
        return $("a[onclick*=addToCart]");
    }

}
