### Tools ###

* [Selenide](https://selenide.org/)
* [JUnit5](https://junit.org/junit5/docs/current/user-guide/)
* [Maven](https://maven.apache.org/)
* [Cucumber](https://cucumber.io/)

### Requirements ###

* [OpenJDK 11](https://openjdk.java.net/projects/jdk/11/)
* [Maven](https://maven.apache.org/)
* IDE - [IntelliJ](https://www.jetbrains.com/idea/)

### Setup ###

1. Open IntelliJ IDEA -> Press button 'Open' -> Locate and open the project. Maven should automatically download all required dependencies.
2. Follow this [guide](https://www.jetbrains.com/help/idea/managing-plugins.html) to install plugins 'Gherkin' and 'Cucumber for java'
3. Install [lombok plugin](https://projectlombok.org/setup/intellij)

Note:

If you encounter any issues with depdendencies, try to run `mvn clean install -U` in the root of the project.
Delete any existing run configurations in 'Edit/Run Debug configurations' window in IntelliJ, in case tests can not be run via IDE.

### Run features/scenarios in IntelliJ ###

Feature files are located in *src/test/java/features* directory.
Each feature file contains one or more scenarios.

```gherkin
Feature: Product purchase

  Background:
    Given I have opened the home page

  @smoke
  Scenario: Add products in cart
    Given navigate to "Phones" category
    And select product "Nexus 6"
    And add the product to the cart
    And click on "Home" section
    And navigate to "Phones" category
    And select product "Samsung Galaxy S7"
    And add the product to the cart
    When click on "Cart" section
    Then total price in cart should be 1450
    And count of the products in cart should be 2
```

If you want to run a particular feature or scenario, simply right click on feature file or scenario -> Run feature/scenario.

### Run features/scenarios in CLI ###

Navigate to the root directory of the project and use the following maven command to run all tests in the current project:

`mvn clean test`

You can also run scenarios by tag this way

`mvn test -Dcucumber.filter.tags="@smoke and @full"`

Currently, headless execution is set to false by default in *src/main/resources/selenide.properties.*
If you want to execute tests in headless mode, use the following selenide option:

`mvn test -Dselenide.headless=true`

Further options can be explored in [Maven](https://maven.apache.org/), [Cucumber](https://cucumber.io/) and [Selenide](https://selenide.org/) official documentation.

### Reporting ###

After the execution of tests either via CLI or IntelliJ IDEA using the `CucumberRunner.class`, a cucumber report `cucumber-pretty.html` will be generated under *target/site* directory.